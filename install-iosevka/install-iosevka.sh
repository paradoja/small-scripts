#!/bin/env bash
# nix shell nixpkgs#curl nixpkgs#jq nixpkgs#wget
set -eo pipefail

ghjson=$(curl --silent "https://api.github.com/repos/be5invis/Iosevka/releases/latest")

tmpDir="$HOME/tmp/iosevka-font/$(echo "$ghjson" | jq -r .tag_name)"
mkdir -p "$tmpDir"
cd "$tmpDir"

# for each set of fonts, there are (and the names start with):
# - ttc - normal ttc; check following two lines
# - super-ttc - ttc with diffferent weights and variants https://github.com/be5invis/Iosevka/issues/332
# - ttc-sgr - single spacing variant https://github.com/be5invis/Iosevka/issues/1112
# - ttf - normal ttf font + webfonts
# - ttf-unhinted - ttf without hints for small resolutions (smaller size)
# - webfont - webfont only (css + woff2)
# also check https://github.com/be5invis/Iosevka/blob/master/doc/custom-build.md
#
# apart from this there's teh -ssxx- variants for each font
for assetList in $(IFS=$'\n' echo "$ghjson" | jq -c '.assets[] | select(.name | test("IosevkaTerm")) | [.name,.browser_download_url]'); do
    read -ra asset <<< "$(echo "$assetList" | column -ts '[],"')"
    name="${asset[0]}"
    downloadUrl="${asset[1]}"

    echo "Downloading $name"
    wget -nv "$downloadUrl"
    unzip "$name"
done

finalDir="/usr/local/share/fonts/iosevka-font"
echo "Creating $finalDir; requires sudo"
sudo mkdir -p "$finalDir"
sudo mv ./*.tt? "$finalDir"

echo "Updating font cache; requires sudo"
sudo fc-cache -fv

# echo "Left lots of crap in $tmpDir"
rm -rf "$tmpDir"
